brightenedLights = [];
function updateDimLightSourcesWithinVision() {
    //Do nothing if global illumination is enabled
    if (canvas.lighting.hasGlobalIllumination()) return;  
    //iterate over all lighting sources
    for ( let source of canvas.lighting.sources ) {
        //ignore darkness sources and exclusively bright/dim sources
        if (source.darkness == true || source.bright > source.dim  || source.bright == 0) continue; 

        //iterate over controlled tokens
        for(let token of canvas.tokens.controlled) {
            //ignore tokens without dimsight
            if (!token.data.dimSight) continue;

            //Find distance between token and light source            
            var scale = canvas.dimensions.size / canvas.dimensions.distance;
            var distance = Math.hypot(token.center.x - source.x, token.center.y - source.y) / scale;
            distance = distance - source.dim / scale;
            dimsightdistance = token.getLightRadius(token.data.dimSight) / scale;

            //Find light sources within vision
            if (distance <= dimsightdistance && (!brightenedLights.includes(source))) {
                //Save this light source in a list.  Only need to update a light source once
                brightenedLights.push(source);

                //Create a copy of the light source, but only keep the bright part
                var sourceBright = new PointSource();
                sourceBright.initialize({
                    x: source.x,
                    y: source.y,
                    z: 0,
                    dim: 0,
                    bright: source.bright,
                    angle: source.angle,
                    rotation: source.rotation,
                    color: source.color,
                    alpha: source.alpha,
                    animation: source.animation,
                    seed: source.seed,
                    darknessThreshold: source.darknessThreshold,
                    type: source.type
                  });
                  

                //Draw the copy on the canvas in such a way that it does not blend with other light
                drawLightSource(sourceBright);

                //Turn the original light into a purely dim light
                source.originalratio = source.ratio;
                source.originalbright = source.bright;
                source.ratio = 0;
                source.bright = 0;
                //Refresh the source
                updateLightSource(source);                
                //Set source back to normal after it is drawn
                source.ratio = source.originalratio;
                source.bright = source.originalbright;
                source._resetIlluminationUniforms = true;
                source._resetColorationUniforms = true;
            }
        }
    }
}

//Refresh a single light source
function updateLightSource(source) {
    source._resetIlluminationUniforms = true;
    source.drawLight();
    source._resetColorationUniforms = true;
    source.drawColor();
}

//Draw a light source directly on top of the canvas, which for some reason does not blend with sight sources...
function drawLightSource(source) {
    var renderedLights = canvas.lighting.illumination.lights;
    renderedLights.addChild(source.drawLight(canvas.lighting.channels));
}


var updatedTokens = [];
var dimsightcolor = [];
var darkvisionbrightness = 1;
function darkenTokenVisions() {
    //Iterate over all tokens
    for ( let token of canvas.tokens.objects.children ) { 
        if (!updatedTokens.includes(token)) {
            //Override PointSource.drawLight() for this token
            var _drawLight = token.vision.drawLight;            
            token.vision.drawLight = function() {   
                //Do nothing if global illumination is enabled, or token has no dimsight
                if (canvas.lighting.hasGlobalIllumination() || !token.data.dimSight) return _drawLight.apply(this);  

                //Calculate the color of dimsight so that dimsight + dim light = bright light
                const channels = canvas.lighting.channels;
                var originaldim = [];
                dimsightcolor = [];
                for(var i = 0; i<= channels.dim.rgb.length-1;i++) {
                    dimsightcolor.push(darkvisionbrightness * (channels.bright.rgb[i] - channels.dim.rgb[i]));
                }                

                //Set blendmode of this token's dimsight to ADD  (adds this tokens dimsight color to the color of light sources underneath)
                token.vision.illumination.light.blendMode = PIXI.BLEND_MODES.ADD; 

                //Temporarily set the color of dim light
                originaldim = canvas.lighting.channels.dim.rgb
                canvas.lighting.channels.dim.rgb = dimsightcolor;  
                //Draw the dimsight of the token
                token.vision._resetIlluminationUniforms = true;
                var returnvalue = _drawLight.apply(this);
                //Set the color of dim light back
                canvas.lighting.channels.dim.rgb = originaldim;  
                
                return  returnvalue;
            }
            updatedTokens.push(token);
        }
    }
}


Hooks.on("sightRefresh", function() {
   updateDimLightSourcesWithinVision();
});

Hooks.on("lightingRefresh", function() {
    brightenedLights = [];
 });

Hooks.on("canvasReady", function() {
    console.log("New Canvas - updating tokens");
    darkenTokenVisions();    
});

Hooks.on("createToken", function() {
    console.log("New token created, updating tokens");
    darkenTokenVisions();    
});

Hooks.on("ready", function() {
    game.settings.register("Darkvision5E", "dimVisionBrightness", {
        name: "Dim vision brightness",
        hint: "Change how bright the default dark vision is (only used for tokens with dim vision)",
        scope: "world",
        config: true,
        type: Number,
        range: {             // If range is specified, the resulting setting will be a range slider
            min: 0,
            max: 3,
            step: 0.05
          },
        default: "1",
        onChange: value => {
            console.log("Darkvision brightness set to: " + value);
            darkvisionbrightness = value;
        }
      });
    
      darkvisionbrightness = game.settings.get("Darkvision5E", "dimVisionBrightness");

});

console.log("Darkvision5E file loaded");